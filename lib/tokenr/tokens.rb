require 'terminal'
require 'tokens_helpers'

class Tokens

	def initialize(tokens_pattern='*.master.{config,tt}', globals_file='./globals.rb', writer=TokensWriter.new, checker=TokensChecker.new)
		@globals_file = globals_file
		@tokens_pattern = tokens_pattern
		@writer = writer
		@checker = checker
	end

	def replace(path, default_environment='local')
		puts " _        _                   "
		puts "| |_ ___ | | _____ _ __  _ __ "
		puts "| __/ _ \\| |/ / _ \\ '_ \\| '__|"
		puts "| || (_) |   <  __/ | | | |   "
		puts " \\__\\___/|_|\\_\\___|_| |_|_|   "
		puts
		
		find_master_files_in(path).each do |file|
			puts '', file
			read_global_variables
			read_file_variables(file)		
			only_use_environments_defined_in_tokens_rb		
			
			@checker.check_for_duplicates(@environments)
			default = set_default_environment(default_environment)
			@environments.each_pair { |environment, values| values[:Environment] = environment }

			# Write out a file for each environment
			if(@environments.keys.length > 1)
				@environments.each_pair do |environment, values| 
					@writer.write_for(environment, file, values, @globals[environment], true) 
				end
			end
			
			# Write out a file for the default environment
			@writer.write_for('', file, @environments[default], @globals[default])
		end
	end

	private

	def read_global_variables
		read_tokens(@globals_file)
		@globals = @environments.dup	
	end

	def read_file_variables(file)
		read_tokens(file.gsub('.master' + File.extname(file), '.tokens.rb'))	
	end

	def set_default_environment(environment)
		default = environment

		if !@environments.has_key?(environment)
			puts "No default environment: #{environment}. Default environment set to: #{@environments.keys.first}"
			default = @environments.keys.first.to_s
		end
		
		return default		
	end

	def find_master_files_in(path)
		return Dir.glob("#{path}/**/#{@tokens_pattern}", File::FNM_CASEFOLD)
	end

	def read_tokens(path)
		@environments = Hash.new({})

		if(File.exist?(path))
			begin			
				eval(File.read(path))
			rescue SyntaxError => exception
				raise exception, "Syntax error in the tokens file #{path} - #{exception.message}".red
			end		
		else
			puts "No tokens file found at #{path}".yellow
		end	
	end

	def only_use_environments_defined_in_tokens_rb()	
		@globals.each_key do |key|
			@globals.delete(key) if !@environments.has_key?(key)
		end
		
		puts "Environments found - #{@environments.keys.join(', ')}"
	end
end
