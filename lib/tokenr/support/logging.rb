require 'logger'

module Logging
	def logger
		@logger ||= Logging.logger_for(self.class.name)
	end

	# Use a hash class-ivar to cache a unique Logger per class:
	@loggers = {}

	class << self
		def logger_for(classname)
			@loggers[classname] ||= configure_logger_for(classname)
		end

		def configure_logger_for(classname)
			level = Logger::ERROR

			if(defined?(LOGGING_LEVEL))
				level = LOGGING_LEVEL
			end			

			logger = Logger.new(STDOUT)
			logger.level = level
			logger.progname = classname
			logger
		end
	end
end
