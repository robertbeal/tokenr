
class TokensChecker

	def check_for_duplicates(environments)
		first_hash = environments[environments.keys.first]
		
		first_hash.each_pair do |key, value|
			environments.keys.each do |environment|
				# Skip if what we're comparing is actually the first environment				
				next if environment == environments.keys.first

				break if environments[environment][key] != value
				
				if(environment == environments.keys.last) 
				    puts "Unrequired token - all environments have the same value '#{value}' for key '#{key}'"
				end	
			end
		end		
	end
end
