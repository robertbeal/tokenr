
class TokensWriter

	def write_for(environment, master_file, tokens, globals, check_usage=false)		
		@environment = environment

		output_file = master_file.gsub('.master', environment.empty? ? '' : ".#{environment}")
		text = File.read(master_file)
		
		check_if_tokens_are_used(text, tokens) if check_usage
		replace_tokens(text, tokens)
		replace_tokens(text, globals)

		write_to_file(output_file, text)        

		matches = text.scan(/(\$[\w\d\-_\.]+\$)/)

		if (matches.any?)
			raise UnReplacedTokenException, "There are untokenised value(s): #{matches.join(', ').strip} still in #{output_file}!".red
		end
	end

	private
	
	def replace_tokens(text, tokens)		
		tokens.each_pair { |key, value|	text.gsub!(/\$#{key}\$/, value.to_s) }
	end

	def check_if_tokens_are_used(text, tokens)
		tokens.each { |key, value| print_unused_token_warning(key) if !text.include?("$#{key}$") && key != :Environment }
	end

	def print_unused_token_warning(key)
		puts "$#{key}$ token is not used for '#{@environment}'".yellow 
	end

	def write_to_file(file, text)
		File.open(file, 'w') { |f| f.write(text) } 
	end

end

class UnReplacedTokenException < StandardError  
end 

