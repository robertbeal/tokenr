
def keys_for(environment, values)
	environment = environment.to_s.downcase

	if @environments.has_key?(environment)
		@environments[environment] = @environments[environment].merge(values)
	else
		@environments[environment] = values
	end
end

def share_with(environments, values)
	environments.each { |environment| keys_for(environment, values) }
end
