require 'support/spec_helper'

describe 'TokensHelper' do

	context 'Given environment values' do
		before(:each) do
			@environments = {}
		end

		it 'adds an environment when one does not exist' do
			keys_for(:dev, { :value => 'test' })
			@environments.keys.count.should == 1
			@environments['dev'].should == { :value => 'test' }
		end
		
		it 'adds a value to an existing environment' do
			@environments['dev'] = { :first => 'test-first' }
			keys_for(:dev, { :second => 'test-second' })
			
			@environments.keys.count.should == 1
			@environments['dev'].should == { 
				:first => 'test-first',
				:second => 'test-second',
			}
		end
		
		it 'overwrites an existing value for an environment' do
			@environments['dev'] = { :value => 'first' }
			keys_for(:dev, { :value => 'second' })
			@environments['dev'].should == { :value => 'second' }
		end
		
		it 'merges hashes without affecting other environments' do
			keys_for(:dev, { :value => 'second' })
			keys_for(:uat, { :value => 'second' })
			keys_for(:dev, { :value => 'third', :name => 'test' })
			
			@environments['dev'].should == { :value => 'third', :name => 'test' }
			@environments['uat'].should == { :value => 'second' }						
		end
	end
	
	context 'Given values are being shared across environments' do
		before(:each) do
			@environments = {}
		end	
	
		it 'adds a value to each environment' do
			share_with [:local, :dev], { :value => 'test' }
			share_with [:uat, :prod], { :name => 'example' }
			share_with [:local, :dev, :uat, :prod], :all => 'true'
			
			@environments.keys.count.should == 4
			
			@environments['local'].should == { :value => 'test', :all => 'true' }
			@environments['dev'].should == { :value => 'test', :all => 'true' }	
			@environments['uat'].should == { :name => 'example', :all => 'true' }		
			@environments['prod'].should == { :name => 'example', :all => 'true' }
		end
	end
end
