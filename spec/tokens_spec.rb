require 'support/spec_helper'
require 'tokens'

shared_examples_for 'tokens' do
	before(:each) do
		@environment = 'test'
		@globals_file = './globals.rb'
		@master_file = './project/Web.master.config'
		@tokens_file = './project/Web.tokens.rb'

		@writer = mock()
		
		@checker = mock()
		@checker.stubs(:check_for_duplicates)

		@tokens = Tokens.new('*.master.*', @globals_file, @writer, @checker)
		#@tokens.stubs(:puts).returns('')
		@tokens.stubs(:print_heading).returns('')

		Dir.stubs(:glob).with('./temp/**/*.master.*', File::FNM_CASEFOLD).returns([@master_file])

		File.stubs(:exist?).with(@globals_file).returns(true)
		File.stubs(:exist?).with(@tokens_file).returns(true)
	end

	def given_the_file_contains(file, text)
		File.stubs(:read).with(file).returns(text)
	end

	def execute()
		@tokens.replace('./temp', @environment)
	end	
end

describe Tokens do
	include_context 'tokens'

	context 'Given an invalid tokens file' do
		before(:each) do
			given_the_file_contains(@globals_file, '')
			given_the_file_contains(@tokens_file, 'crappy syntax :example => "dev"  }')
		end

		it 'raises an exception if the file has syntax errors' do
			expect{ execute() }.to raise_error SyntaxError
		end
	end

	context 'Given an invalid globals file' do
		before(:each) do
			given_the_file_contains(@globals_file, 'crappy syntax :example => "dev"  }')
		end		

		it 'raises an exception if the file has syntax errors' do
			expect{ execute() }.to raise_error SyntaxError
		end
	end

	context 'Given a valid tokens file' do
		before(:each) do
			given_the_file_contains(@tokens_file, '
				keys_for :dev,  { :example => "dev"  } 
				keys_for :test, { :example => "test"  }')
			given_the_file_contains(@globals_file, '')
			
			@writer.expects(:write_for).with('dev',  @master_file, { :example => 'dev',  :Environment => 'dev'  }, {}, true)
			@writer.expects(:write_for).with('test', @master_file, { :example => 'test', :Environment => 'test' }, {}, true)			
		end

		it 'defaults to the first environment when a default environment does not exist' do
			@environment = 'does_not_exist'
			@writer.expects(:write_for).with('', @master_file, { :example => 'dev', :Environment => 'dev' }, {})
			execute			
		end

		it 'tokenises for all environments' do	
			@environment = 'test'
			@writer.expects(:write_for).with('', @master_file, { :example => 'test', :Environment => 'test' }, {})
			execute
		end
	end

	context 'Given a valid tokens and globals file' do
		before(:each) do
			given_the_file_contains(@globals_file, 'keys_for :live, { :example => "live" }')
			given_the_file_contains(@tokens_file,  'keys_for :test, { :example => "test" }')
		end

		it 'only tokenises for environments in the tokens file, additional global environments (ie "live") are excluded' do
			@writer.expects(:write_for).with('live', @master_file, { :example => 'test', :Environment => 'live' }, {}, true).never
			@writer.expects(:write_for).with('',     @master_file, { :example => 'test', :Environment => 'test' }, {})
			execute
		end
	end
	
	context 'Given there are only globals tokens' do
		before(:each) do
			given_the_file_contains(@globals_file, '
				keys_for :test, { :example => "test" }
				keys_for :live, { :example => "live" }')
			given_the_file_contains(@tokens_file,  'keys_for :test, {}')
		end

		it 'only applies environments based on environments specified in the tokens file' do
			@writer.expects(:write_for).with('',     @master_file, { :Environment => 'test' }, { :example => 'test' })
			execute
		end
	end	
	
	context 'Given there shared tokens' do
		before(:each) do
			given_the_file_contains(@globals_file, '')
			given_the_file_contains(@tokens_file,  '
				share_with [:dev, :test], { :example => "true" }
				keys_for :dev,  {} 
				keys_for :test, {}')
		end

		it 'only applies environments based on environments specified in the tokens file' do
			@writer.expects(:write_for).with('dev',  @master_file, { :example => 'true', :Environment => 'dev' }, {}, true)
			@writer.expects(:write_for).with('test', @master_file, { :example => 'true', :Environment => 'test' }, {}, true)
			@writer.expects(:write_for).with('',     @master_file, { :example => 'true', :Environment => 'test' }, {})
			execute
		end
	end
	
	context 'Given a single environment' do
		before(:each) do
			given_the_file_contains(@globals_file, '')
			given_the_file_contains(@tokens_file,  'keys_for :dev, { :name => "example" }')
		end	
	
		it 'only writes out the default file' do
			@writer.expects(:write_for).with('dev',  @master_file, { :name => 'example', :Environment => 'dev' }, {}, true).never
			@writer.expects(:write_for).with('',     @master_file, { :name => 'example', :Environment => 'dev' }, {})
			execute		
		end	
	end	
end
