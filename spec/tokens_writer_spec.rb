require 'support/spec_helper'
require 'tokens_writer'

shared_examples_for 'tokens_writer' do
	before(:each) do
		@tokens = {}
		@globals = {}
		@writer = TokensWriter.new
		@writer.stubs(:puts).returns('')
		@check_key_usage = true
		
		File.stubs(:open).returns('')
		File.stubs(:read).returns('Example file $token$')
	end

	def execute_task
		@result = @writer.write_for('dev', './web.master.config', @tokens, @globals, @check_key_usage)
	end
end

describe TokensWriter do
	include_context 'tokens_writer'	

	context 'Given tokens are still in the output file' do
		it 'raises an exception' do	     
			lambda{ execute_task }.should raise_error UnReplacedTokenException
		end
	end

	context 'Given a token key is not used' do
		before(:each) do
			@tokens = { 'token' => 'value', 'not-used' => 'value' }        
		end

		it 'warns the user' do
			@writer.expects(:print_unused_token_warning)        
			execute_task
		end

		it 'does not warn the user when check_key_usage is disabled' do
			@check_key_usage = false			
			@writer.expects(:print_unused_token_warning).never
			execute_task
		end
		
		it 'does not warn the user when the key is ":Environment"' do
			@tokens = { 'token' => 'value', :Environment => 'live' }			
			@writer.expects(:print_unused_token_warning).never
			execute_task
		end		
	end

	context 'Given a duplicate key in tokens and globals' do
		before(:each) do
			@tokens =  { 'token' => 'this-first' }        
			@globals = { 'token' => 'this-second' }       
		end

		it 'applies tokens first before globals' do
			@writer.expects(:write_to_file).with('./web.dev.config', 'Example file this-first')   
			execute_task
		end
	end
end
