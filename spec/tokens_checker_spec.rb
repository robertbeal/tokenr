require 'support/spec_helper'

describe TokensChecker do

	context 'Given token values are the same across all environments' do
		before(:each) do
			
		end

		it 'warns the user' do
			environments = {			
				'dev' => { :my_key => 'same-value'},
				'uat' => { :my_key => 'same-value'},
				'prod' => { :my_key => 'same-value'},
			}
			
			checker = TokensChecker.new
			checker.expects(:puts).with("Unrequired token - all environments have the same value 'same-value' for key 'my_key'").once
			
			checker.check_for_duplicates(environments)
		end
	end

	context 'Given token values are not all the same' do
		before(:each) do
			
		end

		it 'does not warn the user' do
			environments = {			
				'dev' => { :my_key => 'value1'},
				'uat' => { :my_key => 'value2'},
				'prod' => { :my_key => 'value3'},
			}
			
			checker = TokensChecker.new
			checker.expects(:puts).never
			
			checker.check_for_duplicates(environments)
		end
	end
end
